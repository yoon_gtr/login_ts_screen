import React, { useState } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import axios from 'axios';

import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
    title: {
      fontSize: 30,
      color: '#333'
    },
    formWrap: {
      '& > *:nth-child(n+1)': {
        marginTop: '20px',
        boxShadow: 'none'
      },
      '& input': {
        height: 17,
        fontSize: 15,
        letterSpacing: '1px'
      },
      '& .MuiButton-contained': {
        color: 'white'
      }
    },
    button: {
      margin: theme.spacing(1, 0),
      height: '54px',
      backgroundColor: '#16a4b6',
      '& span':{
        fontSize: 20
      }
    }
   
  }),
);

const errorsTotal = {
  email: { state: false, msg: '' },
  password: { state: false, msg: '' },
}

function Login() {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState(errorsTotal);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name } = event.target
  }

  const signIn = () => {

  }

  return(
    <div className={classes.root}>
      <div className={classes.title}>
        GTR LOGIN
      </div>
      <div className={classes.formWrap}>
        <TextField
          variant="outlined"
          fullWidth
          type='email'
          placeholder={'이메일 주소를 입력해 주세요.'}
          name="email"
          value={email}
          error={errors.email.state}
          helperText={errors.email.msg}
          onChange={e => handleChange(e)}
        />
        <TextField
          variant="outlined"
          fullWidth
          type='password'
          placeholder={'비밀번호를 입력해 주세요.'}
          name="password"
          value={password}
          error={errors.password.state}
          helperText={errors.password.msg}
          onChange={e => handleChange(e)}
        />
        <Button 
          variant="contained"
          fullWidth
          className={classes.button}
          onClick={() => signIn()}
        >
          로그인
        </Button>
      </div>
    </div>
  )
}

export default Login